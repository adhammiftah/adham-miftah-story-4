from django.contrib import admin
from .models import ClassYear, Friends

# Register your models here.

admin.site.register(ClassYear)
admin.site.register(Friends)