from django.shortcuts import render
from .forms import TheForm
from .models import Friends
# Create your views here.

def index(request):
	return render(request, 'story/index.html')

def about(request):
	return render(request, 'story/about.html')

def contact(request):
	return render(request, 'story/contact.html')

def forms(request):
	if request.method == 'POST':
		form = TheForm(request.POST)
		if form.is_valid():
			data_item = form.save(commit=False)
			data_item.save()
			return render(request, 'story/thanks.html')
	form = TheForm()

	return render(request, 'story/forms.html', {'form':form})

def friendlist(request):
	allfriends = Friends.objects.all()
	return render(request, 'story/friendlist.html', {'allfriends':allfriends})

def thanks(request):
	return render(request, 'story/thanks.html')