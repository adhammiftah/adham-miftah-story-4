from django.db import models

# Create your models here.

class ClassYear(models.Model):
	year = models.IntegerField(primary_key=True)

	def __str__(self):
		return '%s' % self.year


class Friends(models.Model):
	name = models.CharField(max_length=255, default='', blank=True)
	the_year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)
	hobby = models.CharField(max_length=255, default='', blank=True)
	fnb = models.CharField(max_length=255, default='', blank=True)

	def __str__(self):
		return '%s' % self.name